<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Function</title>
</head>

<body>
<h1>Berlatih Function PHP</h1>
<?php

echo "<h3> nested looping pagar bintang </h3>";
/* 
Soal No 1
Greetings
Buatlah sebuah function greetings() yang menerima satu parameter berupa string. 

contoh: greetings("abduh");
Output: "Halo Abduh, Selamat Datang di Sanbercode!"
*/
function pagar_bintang($integer){
    //code disini
for ($i=1; $i<=$integer; $i++){
    for ($j=1; $j<=$integer; $j++){
        if($i % 2 == 0){
            echo "*";
        } else {
            echo "#";
        } 
    } echo "<br/>";
}

}

            echo pagar_bintang(5);
            echo pagar_bintang(8);
            echo pagar_bintang(10);
echo "<h3>Soal benar salah xo</h3>";

function xo($str) {

    //Code disini
    echo "Kalimat $str Memiliki : <br>";
    echo "jumlah huruf X : ".substr_count($str,"x")."<br>";
    echo "jumlah huruf O: ".substr_count($str,"o")."<br>";

    $jumlahX=substr_count($str,"o");
    $jumlahO=substr_count($str,"x");

    if ($jumlahX==$jumlahO){
        echo" xo ($str) = Benar <br><br>";
    } else{
        echo "xo ($str) = Salah <br><br>";
    }
    
    };
    
    // Test Cases
    echo xo('xoxoxo'); // "Benar"
    echo xo('oxooxo'); // "Salah"
    echo xo('oxo'); // "Salah"
    echo xo('xxooox'); // "Benar"
    echo xo('xoxooxxo'); // "Benar"

?>

</body>

</html>